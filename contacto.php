<!DOCTYPE html>
<html>

<head>
	<title>Contacto</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Bluefish 2.2.7" />
	
	<link rel="stylesheet" href="css/general.css">
	<link rel="stylesheet" href="css/contacto.css">
</head>

<body>
	
	<div id="contenido">
	<?php 
		session_start();
		$_SESSION['id_contacto']=$_GET['id'];
		
		include('con.php');
		$id_login = $_SESSION['id_login'];
		$nomContactos = "select * from contactos where id_cont = '".$_SESSION['id_contacto']."';";

		
		if($result=mysqli_query($con,$nomContactos)){
		
			if($row=mysqli_num_rows($result)){
				
					for($i=0;$i<$row;$i++){
						$row=mysqli_fetch_array($result);
						if($row['id_cont'] == $_SESSION['id_contacto']){
							echo '<div id="contactoheader">';
							echo '<div class="contactofoto">';
							echo '<img src="img/usuario.jpg" height="142" width="188">';
							echo '</div>';
							
							echo '<h2>'.$row['nombre'].'  '.$row['apellido'].'</h2>';
							echo '</div>';
							
							echo '<div id="contactodatos">';
							echo '<p class="datos">Telefono:<span class="datosintroducidos">'.$row['telefono'].'</span></p>';
							
							echo '<p class="datos">Correo:<span class="datosintroducidos">'.$row['correo'].'</span></p>';
							echo '<p class="datos">Domicilio:<span class="datosintroducidos">'.$row['domicilio'].'</span></p>';
							echo '<p class="datos">Fecha de Nacimiento:<span class="datosintroducidos">'.$row['fecha'].'</span></p>';
							echo '</div>';
								
						
						}
					}
				}
			}
		mysqli_close($con);
	?>
	
	
	<div id="buttons">	
		<span class="button"><a href="modificaContacto.php"><button name ="modificar" type="button">modifica</button></a></span>
		<span class="button"><a href="eliminaContacto.php"><button onclick="eliminar()" name ="eliminar" type="button"> Eliminar</button></a></span>
		<span class="button"><a href="cuenta.php"><img src="img/home.png" height="32" width="39"/></a></span>
	</div>
	</div>
	
	<script type="text/javascript" src="js/popup.js"></script>
</body>

</html>

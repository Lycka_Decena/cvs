CREATE DATABASE a15amidecabi_agenda;

USE a15amidecabi_agenda;

CREATE TABLE login(id_login INT NOT NULL AUTO_INCREMENT,PRIMARY KEY(id_login),nombre VARCHAR(20),password VARCHAR(20),md5 VARCHAR(34))ENGINE = INNODB;
CREATE TABLE contactos(id_cont INT NOT NULL AUTO_INCREMENT PRIMARY KEY, id_login INT NOT NULL,nombre VARCHAR(20),apellido VARCHAR(20),telefono VARCHAR(9),correo VARCHAR(30),domicilio VARCHAR(20),fecha VARCHAR(20),FOREIGN KEY (id_login) REFERENCES login(id_login) ON DELETE CASCADE)ENGINE=INNODB;

insert into login(nombre,password,md5) values('trump','ausias','a247ec7cbe6a7bc111277ad4480e2fde');
insert into login(nombre,password,md5) values('gabriel','ausias','a247ec7cbe6a7bc111277ad4480e2fde');
insert into login(nombre,password,md5) values('lycka','ausias','a247ec7cbe6a7bc111277ad4480e2fde');

insert into contactos(id_login,nombre,apellido,telefono,correo,domicilio,fecha) values(1,'mario','perez','929382739','mario@iam.cat','casa','30/03/1990');
insert into contactos(id_login,nombre,apellido,telefono,correo,domicilio,fecha) values(2,'rumble','lol','923282739','rum@iam.cat','spain','02/09/1990');
insert into contactos(id_login,nombre,apellido,telefono,correo,domicilio,fecha) values(3,'laura','paussini','929382739','lau@iam.cat','sicilia','21/07/1990');

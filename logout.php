<?php

	session_start();
	unset($_SESSION['usuario']);
	session_destroy();

	//La pagina que aparecerá cuando hace logout

	header("Location: login.php");

?>

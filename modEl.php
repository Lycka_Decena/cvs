<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modificar Eliminar</title>

<link rel="stylesheet" href="css/general.css">
<link rel="stylesheet" href="css/modificarAnadir.css">
<link rel="stylesheet" href="css/calendario.css">
<script type="text/javascript" src="js/calendario.js"></script>
</head>
<body>
	<?php
		//fecha
		
		$dia = $_GET['dia'];
		$mes = $_GET['mes']+1;
		$any = $_GET['any'];
			
		if(!$_POST){
	?>

	<div class="container">
		
		<h2>Nuevo Usuario</h2>
		
		<form class="formulario" action="modEl.php" method="POST">
		
			<div class="field">
				<img src="img/usuario.png" alt="user"/>
				<input class="text" type="text" placeholder="Introduce tu nombre" name="nom" required>
			</div>
			
			<div class="field">
				<img src="img/usuario.png" alt="lastname"/>
				<input class="text" type="text" placeholder="Introduce tu apellido" name="ape" required>
			</div>
			
			<div class="field">	
				<img src="img/phone.png" alt="phone">			
				<input class="text" type="text" placeholder="Introduce tu telefono" name="tel" required>

			</div>
			
			<div class="field">
					<img src="img/mail.png" alt="mail">
					<input class="text" type="text" placeholder="Introduce tu correo" name="mail" required>
			</div>
			
			<div class="field">
				<img src="img/home.png" alt="home">
				<input class="text" type="text" placeholder="introduce tu domicilio" name="dom" required>
			</div>
			
			
			<div class="field">
				<img src="img/calendar.png" alt="calendar">			
				<input class="text" type="text" placeholder="dd/mm/yyyy" onclick="calendario()" name="fecha" value="<?php echo $dia."/".$mes."/".$any?>">
				<p id="calendari"></p>
			</div>
			
		<div class="button"> 
		<input class="guardar" type="submit" value="Guardar" onclick="modificar()">
		<a href="cuenta.php"><img id="home" src="img/home.png" height="32" width="39"/></a>
		</div>
		
		</form>
	</div>

	<?php 
		}else{
			session_start();
			include('con.php');
			$id_login = $_SESSION['id_login'];
			
			$id_contacto = $_SESSION['id_contacto'];
			$nom = $_POST["nom"];
			$apellido = $_POST["ape"];
			$telefono = $_POST["tel"];
			$email = $_POST["mail"];
			$domicilio = $_POST["dom"];
			$fecha = $_POST["fecha"];
			
			$search = "insert into contactos(id_login,nombre,apellido,telefono,correo,domicilio,fecha) values('".$id_login."','".$nom."','".$apellido."','".$telefono."','".$email."','".$domicilio."','".$fecha."');";
			
			if($result=mysqli_query($con,$search)){
				echo "se ha creado correctamente los siguientes datos: <br>";
				echo "[Nombre: ".$nom."] [Apellido: ".$apellido."] [Telefono: ".$telefono."] [Email: ".$email."] [Domicilio: ".$domicilio."] [Fecha: ".$fecha."]";
						
			}
			echo '<span class="button"><a href="cuenta.php"><img src="img/home.png" height="32" width="39"/></a></span>';

			mysqli_close($con);
		}
	?>
	
	<script type="text/javascript" src="js/popup.js"></script>

</body>
</html> 
